plugins {
    id("com.android.application")
}

android {
    namespace = "com.umayuxsolutions.novenamilagro"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.umayuxsolutions.novenamilagro"
        minSdk = 19
        targetSdk = 34
        versionCode = 1
        versionName = "2.0"
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

dependencies {
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("com.google.android.material:material:1.11.0")

    implementation("com.github.warkiz.widget:indicatorseekbar:2.1.2")
}