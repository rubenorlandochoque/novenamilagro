package com.umayuxsolutions.novenamilagro.views;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.umayuxsolutions.novenamilagro.R;

public class HimnosActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_himnos);
        findViewById(R.id.himnosr).setOnClickListener(v -> {
            Intent intent = new Intent(HimnosActivity.this, MusicActivity.class);
            intent.putExtra("html", "audio_sr.html");
            startActivity(intent);
        });
        findViewById(R.id.himnovirgen).setOnClickListener(v -> {
            Intent intent = new Intent(HimnosActivity.this, MusicActivity.class);
            intent.putExtra("html", "audio_virgen.html");
            startActivity(intent);
        });
    }
}