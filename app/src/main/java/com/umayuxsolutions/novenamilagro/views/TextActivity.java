package com.umayuxsolutions.novenamilagro.views;

import android.os.Bundle;
import android.webkit.WebView;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.umayuxsolutions.novenamilagro.R;
import com.umayuxsolutions.novenamilagro.helpers.PreferenceHelper;

public class TextActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void setEvents(TextListener textListener) {
        WebView wv = findViewById(R.id.webView1);
        findViewById(R.id.zoom_in).setOnClickListener(view -> {
            int zoomLevel = PreferenceHelper.getInstance().getZoomLevel();
            if (zoomLevel < 50) {
                zoomLevel++;
            }
            PreferenceHelper.getInstance().setZoomLevel(zoomLevel);
            wv.evaluateJavascript("javascript:fontSize(" + zoomLevel + ");", null);
        });

        findViewById(R.id.zoom_out).setOnClickListener(view -> {
            int zoomLevel = PreferenceHelper.getInstance().getZoomLevel();
            if (zoomLevel > 8) {
                zoomLevel--;
            }
            PreferenceHelper.getInstance().setZoomLevel(zoomLevel);
            wv.evaluateJavascript("javascript:fontSize(" + zoomLevel + ");", null);
        });

        findViewById(R.id.brightness).setOnClickListener(view -> {
            PreferenceHelper.getInstance().setBrightness(!PreferenceHelper.getInstance().getBrightness());
            wv.evaluateJavascript("javascript:brightness(" + PreferenceHelper.getInstance().getBrightness() + ");", null);

            if (PreferenceHelper.getInstance().getBrightness()) {
                findViewById(R.id.bnv).setBackground(ContextCompat.getDrawable(this, R.drawable.botonera_light));
                findViewById(R.id.brightness).setBackground(ContextCompat.getDrawable(this, R.drawable.b_selector_light));
                ((ImageButton) findViewById(R.id.brightness)).setImageResource(R.drawable.brightness_light);

                findViewById(R.id.zoom_in).setBackground(ContextCompat.getDrawable(this, R.drawable.b_selector_light));
                ((ImageButton) findViewById(R.id.zoom_in)).setImageResource(R.drawable.zoom_in_light);

                findViewById(R.id.zoom_out).setBackground(ContextCompat.getDrawable(this, R.drawable.b_selector_light));
                ((ImageButton) findViewById(R.id.zoom_out)).setImageResource(R.drawable.zoom_out_light);
            } else {
                findViewById(R.id.bnv).setBackground(ContextCompat.getDrawable(this, R.drawable.botonera_dark));
                findViewById(R.id.brightness).setBackground(ContextCompat.getDrawable(this, R.drawable.b_selector_dark));
                ((ImageButton) findViewById(R.id.brightness)).setImageResource(R.drawable.brightness_dark);

                findViewById(R.id.zoom_in).setBackground(ContextCompat.getDrawable(this, R.drawable.b_selector_dark));
                ((ImageButton) findViewById(R.id.zoom_in)).setImageResource(R.drawable.zoom_in_dark);

                findViewById(R.id.zoom_out).setBackground(ContextCompat.getDrawable(this, R.drawable.b_selector_dark));
                ((ImageButton) findViewById(R.id.zoom_out)).setImageResource(R.drawable.zoom_out_dark);
            }

            if (textListener != null) {
                textListener.onSetEventListner();
            }

        });
    }

    public void setDefaultValues() {
        WebView wv = findViewById(R.id.webView1);
        wv.evaluateJavascript("javascript:brightness(" + PreferenceHelper.getInstance().getBrightness() + ");", null);
        wv.evaluateJavascript("javascript:fontSize(" + PreferenceHelper.getInstance().getZoomLevel() + ");", null);

        if (PreferenceHelper.getInstance().getBrightness()) {
            findViewById(R.id.bnv).setBackground(ContextCompat.getDrawable(this, R.drawable.botonera_light));
            findViewById(R.id.brightness).setBackground(ContextCompat.getDrawable(this, R.drawable.b_selector_light));
            ((ImageButton) findViewById(R.id.brightness)).setImageResource(R.drawable.brightness_light);

            findViewById(R.id.zoom_in).setBackground(ContextCompat.getDrawable(this, R.drawable.b_selector_light));
            ((ImageButton) findViewById(R.id.zoom_in)).setImageResource(R.drawable.zoom_in_light);

            findViewById(R.id.zoom_out).setBackground(ContextCompat.getDrawable(this, R.drawable.b_selector_light));
            ((ImageButton) findViewById(R.id.zoom_out)).setImageResource(R.drawable.zoom_out_light);
        } else {
            findViewById(R.id.bnv).setBackground(ContextCompat.getDrawable(this, R.drawable.botonera_dark));
            findViewById(R.id.brightness).setBackground(ContextCompat.getDrawable(this, R.drawable.b_selector_dark));
            ((ImageButton) findViewById(R.id.brightness)).setImageResource(R.drawable.brightness_dark);

            findViewById(R.id.zoom_in).setBackground(ContextCompat.getDrawable(this, R.drawable.b_selector_dark));
            ((ImageButton) findViewById(R.id.zoom_in)).setImageResource(R.drawable.zoom_in_dark);

            findViewById(R.id.zoom_out).setBackground(ContextCompat.getDrawable(this, R.drawable.b_selector_dark));
            ((ImageButton) findViewById(R.id.zoom_out)).setImageResource(R.drawable.zoom_out_dark);
        }
    }
}
