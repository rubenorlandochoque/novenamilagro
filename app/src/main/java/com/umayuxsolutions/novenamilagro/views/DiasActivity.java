package com.umayuxsolutions.novenamilagro.views;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.umayuxsolutions.novenamilagro.R;

public class DiasActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dias);
        findViewById(R.id.dia_1).setOnClickListener(v -> openNovena("dia1"));
        findViewById(R.id.dia_2).setOnClickListener(v -> openNovena("dia2"));
        findViewById(R.id.dia_3).setOnClickListener(v -> openNovena("dia3"));
        findViewById(R.id.dia_4).setOnClickListener(v -> openNovena("dia4"));
        findViewById(R.id.dia_5).setOnClickListener(v -> openNovena("dia5"));
        findViewById(R.id.dia_6).setOnClickListener(v -> openNovena("dia6"));
        findViewById(R.id.dia_7).setOnClickListener(v -> openNovena("dia7"));
        findViewById(R.id.dia_8).setOnClickListener(v -> openNovena("dia8"));
        findViewById(R.id.dia_9).setOnClickListener(v -> openNovena("dia9"));
    }

    private void openNovena(String dia) {
        Intent intent = new Intent(DiasActivity.this, NovenaActivity.class);
        intent.putExtra("dia", dia);
        startActivity(intent);
    }
}