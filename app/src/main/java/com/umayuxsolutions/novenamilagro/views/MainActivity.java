package com.umayuxsolutions.novenamilagro.views;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.umayuxsolutions.novenamilagro.R;
import com.umayuxsolutions.novenamilagro.helpers.PreferenceHelper;
import com.umayuxsolutions.novenamilagro.texts.Clasica;
import com.umayuxsolutions.novenamilagro.texts.Moderna;
import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.OnSeekChangeListener;
import com.warkiz.widget.SeekParams;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class MainActivity extends AppCompatActivity {
    boolean popupOpen = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.novena).setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, DiasActivity.class);
            startActivity(intent);
        });
        findViewById(R.id.pacto).setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, PactoActivity.class);
            startActivity(intent);
        });

        findViewById(R.id.himno).setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, HimnosActivity.class);
            startActivity(intent);
        });
        findViewById(R.id.configuracion).setOnClickListener(v -> openConfig());
    }

    private void openConfig() {
        popupOpen = true;
        findViewById(R.id.dias_novena).setVisibility(View.GONE);
        findViewById(R.id.configuracion).setVisibility(View.GONE);
        Rect displayRectangle = new Rect();
        Window window = getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.CustomAlertDialog);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this).inflate(R.layout.config_view, viewGroup, false);
        dialogView.setMinimumWidth((int)(displayRectangle.width() * 1f));
        dialogView.setMinimumHeight((int)(displayRectangle.height() * 1f));
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();

        WebView wv = dialogView.findViewById(R.id.demo);
        wv.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                if (newProgress == 100) {
                    wv.evaluateJavascript("javascript:fontSize(" + PreferenceHelper.getInstance().getZoomLevel() + ");", null);
                }
            }
        });
        wv.setBackgroundColor(Color.TRANSPARENT);
        wv.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.loadUrl("file:///android_asset/letra.html");   // now it will not fail here

        Button buttonOk=dialogView.findViewById(R.id.buttonOk);
        IndicatorSeekBar tamanoLetra=dialogView.findViewById(R.id.tamano_letra);
        TextView indicador=dialogView.findViewById(R.id.indicador);
        RadioGroup modoOscuro=dialogView.findViewById(R.id.modo_oscuro);
        RadioButton modoOscuroSi=dialogView.findViewById(R.id.modo_oscuro_si);
        RadioButton modoOscuroNo=dialogView.findViewById(R.id.modo_oscuro_no);
        RadioGroup version=dialogView.findViewById(R.id.version);
        RadioButton versionClasica=dialogView.findViewById(R.id.version_clasica);
        RadioButton versionModerna=dialogView.findViewById(R.id.version_moderna);

        indicador.setText(Integer.toString(PreferenceHelper.getInstance().getZoomLevel()));
        tamanoLetra.setProgress(PreferenceHelper.getInstance().getZoomLevel());

        if(PreferenceHelper.getInstance().getVersion().equals(Moderna.class.getName())) {
            versionModerna.setChecked(true);
        }else{
            versionClasica.setChecked(true);
        }

        if(PreferenceHelper.getInstance().getBrightness()) {
            modoOscuroNo.setChecked(true);
        }else{
            modoOscuroSi.setChecked(true);
        }

        tamanoLetra.setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {
                indicador.setText(Integer.toString(seekParams.progress));
                wv.evaluateJavascript("javascript:fontSize(" + seekParams.progress + ");", null);
            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

            }
        });


        buttonOk.setOnClickListener(v -> {
            findViewById(R.id.dias_novena).setVisibility(View.VISIBLE);
            findViewById(R.id.configuracion).setVisibility(View.VISIBLE);

            int modoOscuroCheckedRadioButtonId = modoOscuro.getCheckedRadioButtonId();
            int versionCheckedRadioButtonId = version.getCheckedRadioButtonId();

            if(modoOscuroCheckedRadioButtonId == R.id.modo_oscuro_si) {
                PreferenceHelper.getInstance().setBrightness(false);
            }else{
                PreferenceHelper.getInstance().setBrightness(true);
            }

            if(versionCheckedRadioButtonId == R.id.version_clasica) {
                PreferenceHelper.getInstance().setVersion(Clasica.class.getName());
            }else{
                PreferenceHelper.getInstance().setVersion(Moderna.class.getName());
            }

            PreferenceHelper.getInstance().setZoomLevel(tamanoLetra.getProgress());

            alertDialog.dismiss();
            popupOpen = false;
        });

        alertDialog.setOnDismissListener(dialogInterface -> {
            findViewById(R.id.dias_novena).setVisibility(View.VISIBLE);
            findViewById(R.id.configuracion).setVisibility(View.VISIBLE);
            popupOpen = false;
        });
        alertDialog.show();
    }
}