package com.umayuxsolutions.novenamilagro.views;

import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import com.umayuxsolutions.novenamilagro.R;
import com.umayuxsolutions.novenamilagro.texts.Pacto;

public class PactoActivity extends TextActivity {
    WebView wv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pacto);

        wv = findViewById(R.id.webView1);
        wv.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                if (newProgress == 100) {
                    wv.evaluateJavascript("javascript:addText('" + new Pacto().pacto() + "');", null);
                    setDefaultValues();
                }
            }
        });
        wv.getSettings().setJavaScriptEnabled(true);
        wv.loadUrl("file:///android_asset/template.html");   // now it will not fail here
        setEvents(null);
    }
}