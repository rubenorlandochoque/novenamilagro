package com.umayuxsolutions.novenamilagro.views;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.umayuxsolutions.novenamilagro.R;
import com.umayuxsolutions.novenamilagro.helpers.PreferenceHelper;
import com.umayuxsolutions.novenamilagro.texts.Clasica;
import com.umayuxsolutions.novenamilagro.texts.Moderna;
import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.OnSeekChangeListener;
import com.warkiz.widget.SeekParams;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class NovenaActivity extends TextActivity {
    WebView wv;
    String dia = "";

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novena);

        dia = getIntent().getStringExtra("dia");

        wv = findViewById(R.id.webView1);
        wv.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                if (newProgress == 100) {
                    cargarNovena(dia);
                    setDefaultValues();
                    if (PreferenceHelper.getInstance().getBrightness()) {
                        findViewById(R.id.chapters).setBackground(ContextCompat.getDrawable(NovenaActivity.this, R.drawable.b_selector_light));
                        ((Button) findViewById(R.id.chapters)).setTextColor(ContextCompat.getColorStateList(NovenaActivity.this, R.drawable.text_light));
                        ((Button) findViewById(R.id.chapters)).setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(NovenaActivity.this, R.drawable.format_list_light), null, null, null);

                        findViewById(R.id.version).setBackground(ContextCompat.getDrawable(NovenaActivity.this, R.drawable.b_selector_light));
                        ((Button) findViewById(R.id.version)).setTextColor(ContextCompat.getColorStateList(NovenaActivity.this, R.drawable.text_light));
                        ((Button) findViewById(R.id.version)).setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(NovenaActivity.this, R.drawable.menu_book_light), null, null, null);
                    } else {
                        findViewById(R.id.chapters).setBackground(ContextCompat.getDrawable(NovenaActivity.this, R.drawable.b_selector_dark));
                        ((Button) findViewById(R.id.chapters)).setTextColor(ContextCompat.getColorStateList(NovenaActivity.this, R.drawable.text_dark));
                        ((Button) findViewById(R.id.chapters)).setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(NovenaActivity.this, R.drawable.format_list_dark), null, null, null);

                        findViewById(R.id.version).setBackground(ContextCompat.getDrawable(NovenaActivity.this, R.drawable.b_selector_dark));
                        ((Button) findViewById(R.id.version)).setTextColor(ContextCompat.getColorStateList(NovenaActivity.this, R.drawable.text_dark));
                        ((Button) findViewById(R.id.version)).setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(NovenaActivity.this, R.drawable.menu_book_dark), null, null, null);
                    }
                }
            }
        });
        wv.getSettings().setJavaScriptEnabled(true);

        wv.loadUrl("file:///android_asset/template.html");   // now it will not fail here
        setEvents(() -> {
            if (PreferenceHelper.getInstance().getBrightness()) {
                findViewById(R.id.chapters).setBackground(ContextCompat.getDrawable(this, R.drawable.b_selector_light));
                ((Button) findViewById(R.id.chapters)).setTextColor(ContextCompat.getColorStateList(this, R.drawable.text_light));
                ((Button) findViewById(R.id.chapters)).setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(this, R.drawable.format_list_light), null, null, null);

                findViewById(R.id.version).setBackground(ContextCompat.getDrawable(this, R.drawable.b_selector_light));
                ((Button) findViewById(R.id.version)).setTextColor(ContextCompat.getColorStateList(NovenaActivity.this, R.drawable.text_light));
                ((Button) findViewById(R.id.version)).setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(NovenaActivity.this, R.drawable.menu_book_light), null, null, null);
            } else {
                findViewById(R.id.chapters).setBackground(ContextCompat.getDrawable(this, R.drawable.b_selector_dark));
                ((Button) findViewById(R.id.chapters)).setTextColor(ContextCompat.getColorStateList(this, R.drawable.text_dark));
                ((Button) findViewById(R.id.chapters)).setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(this, R.drawable.format_list_dark), null, null, null);

                findViewById(R.id.version).setBackground(ContextCompat.getDrawable(this, R.drawable.b_selector_dark));
                ((Button) findViewById(R.id.version)).setTextColor(ContextCompat.getColorStateList(NovenaActivity.this, R.drawable.text_dark));
                ((Button) findViewById(R.id.version)).setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(NovenaActivity.this, R.drawable.menu_book_dark), null, null, null);
            }
        });

        findViewById(R.id.version).setOnClickListener(view -> {
            try {
                String version = PreferenceHelper.getInstance().getVersion();
                if (version.equals(Clasica.class.getName())) {
                    version = Moderna.class.getName();
                    ((Button) findViewById(R.id.version)).setText("MODERNA");
                } else {
                    version = Clasica.class.getName();
                    ((Button) findViewById(R.id.version)).setText("ANTIGUA");
                }
                PreferenceHelper.getInstance().setVersion(version);
                Class novena = Class.forName(version);
                Object t = novena.newInstance();
                Method m = novena.getMethod(dia);
                if (m != null) {
                    String s = (String) m.invoke(t);
                    wv.evaluateJavascript("javascript:addText('" + s + "');", null);
                }
            } catch (ClassNotFoundException | IllegalAccessException | InvocationTargetException | InstantiationException | NoSuchMethodException e) {
                e.printStackTrace();
            }
        });

        findViewById(R.id.chapters).setOnClickListener(view -> {
            Rect displayRectangle = new Rect();
            Window window = getWindow();
            window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
            final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.CustomAlertDialog);
            ViewGroup viewGroup = findViewById(android.R.id.content);
            View dialogView = LayoutInflater.from(this).inflate(R.layout.activity_dias, viewGroup, false);
            dialogView.setMinimumWidth((int) (displayRectangle.width() * 1f));
            dialogView.setMinimumHeight((int) (displayRectangle.height() * 1f));
            builder.setView(dialogView);
            final AlertDialog alertDialog = builder.create();

            dialogView.findViewById(R.id.dia_1).setOnClickListener(v -> {cargarNovena("dia1"); alertDialog.dismiss();});
            dialogView.findViewById(R.id.dia_2).setOnClickListener(v -> {cargarNovena("dia2"); alertDialog.dismiss();});
            dialogView.findViewById(R.id.dia_3).setOnClickListener(v -> {cargarNovena("dia3"); alertDialog.dismiss();});
            dialogView.findViewById(R.id.dia_4).setOnClickListener(v -> {cargarNovena("dia4"); alertDialog.dismiss();});
            dialogView.findViewById(R.id.dia_5).setOnClickListener(v -> {cargarNovena("dia5"); alertDialog.dismiss();});
            dialogView.findViewById(R.id.dia_6).setOnClickListener(v -> {cargarNovena("dia6"); alertDialog.dismiss();});
            dialogView.findViewById(R.id.dia_7).setOnClickListener(v -> {cargarNovena("dia7"); alertDialog.dismiss();});
            dialogView.findViewById(R.id.dia_8).setOnClickListener(v -> {cargarNovena("dia8"); alertDialog.dismiss();});
            dialogView.findViewById(R.id.dia_9).setOnClickListener(v -> {cargarNovena("dia9"); alertDialog.dismiss();});

            alertDialog.show();
        });
    }

    private void cargarNovena(String dia) {
        try {
            String version = PreferenceHelper.getInstance().getVersion();
            PreferenceHelper.getInstance().setVersion(version);
            Class novena = Class.forName(version);
            Object t = novena.newInstance();
            Method m = novena.getMethod(dia);
            if (m != null) {
                String s = (String) m.invoke(t);
                wv.evaluateJavascript("javascript:addText('" + s + "');", null);
            }
        } catch (ClassNotFoundException | IllegalAccessException | InvocationTargetException | InstantiationException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        if (PreferenceHelper.getInstance().getVersion().equals(Moderna.class.getName())) {
            ((Button) findViewById(R.id.version)).setText("MODERNA");
        } else {
            ((Button) findViewById(R.id.version)).setText("ANTIGUA");
        }
        String diaNombre = dia.substring(0, 3).toUpperCase().replace("I", "Í");
        String diaNumero = dia.substring(3);
        ((Button) findViewById(R.id.chapters)).setText(String.format("%s %s", diaNombre, diaNumero));
    }
}