package com.umayuxsolutions.novenamilagro.views;

import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.umayuxsolutions.novenamilagro.R;

public class MusicActivity extends TextActivity {
    WebView wv;
    String dia = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);
        dia = getIntent().getStringExtra("html");
        wv = findViewById(R.id.webView1);
        wv.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                if (newProgress == 100) {
                    setDefaultValues();
                }
            }
        });

        wv.getSettings().setJavaScriptEnabled(true);
        wv.loadUrl("file:///android_asset/" + dia);
        setEvents(null);
    }

    @Override
    public void onPause() {
        wv.onPause();
        wv.pauseTimers();
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        wv.resumeTimers();
        wv.onResume();
    }


    @Override
    protected void onDestroy() {
        wv.destroy();
        wv = null;
        super.onDestroy();
    }
}