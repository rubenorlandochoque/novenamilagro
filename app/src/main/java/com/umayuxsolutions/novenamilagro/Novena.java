package com.umayuxsolutions.novenamilagro;

import android.app.Application;

public class Novena extends Application {
    public static final String TAG = Novena.class.getSimpleName();
    private static Novena mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public static synchronized Novena getInstance() {
        return mInstance;
    }
}
