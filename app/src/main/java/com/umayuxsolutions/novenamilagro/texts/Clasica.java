package com.umayuxsolutions.novenamilagro.texts;

public class Clasica {
    public String dia1() {
        return inicio() +
                tituloDia("PRIMERO") +
                citas("Filipenses  3,  17  -  4,3  /  Evangelio  según  san  Mateo  7,7-10  /  Corintios  4,13-18  / Evangelio según san Lucas 11,5-13") +
                tituloOracion() +
                "<p>Vos sois, pues, oh mi Dios, un bien infinito y más de una vez os he trocado por un vil placer del momento. Más, aún cuando os haya despreciado, me ofrecéis todavía el perdón, si yo lo quiero, y me prometéis recibirme en vuestra gracia, si me arrepiento de haberos ofendido. Sí, oh Señor, arrepiéntome de todo corazón de haberos ultrajado tan vilmente, aborrezco mi pecado más que todo otro mal. Y ahora, volviendo a Vos, como lo espero, me recibiréis, y me abrazaréis como amoroso Padre. Yo os lo agradezco, oh infinita bondad, pero necesito de vuestro auxilio.<br>" +
                "No me lo neguéis, Dios mío, y no permitáis que me separe jamás de Vos. No dejará de tentarme el infierno, pero más fuerte sois Vos que el infierno. Sé que si siempre a Vos me recomiendo, jamás me separaré de Vos, y ésta es la gracia que os pido: haced que nunca cese de rogaros como ahora lo hago. Asistidme Señor, dadme la luz, la fuerza, la perseverancia; dadme el paraíso, pero sobre todo dadme vuestro amor, que es el paraíso de las almas. Os amo, bondad infinita, y quiero amaros siempre; escuchadme por amor de Jesucristo, oh María, Vos que sois el refugio de pecadores; socorred a uno que quiere amar a vuestro Dios.<br>" +
                "Dulcísimo Señor del Milagro, perdonad mis pecados, y librad, por vuestra misericordia, a la ciudad de Salta de todo castigo. Concedednos esta gracia, por intercesión de nuestra Protectora, vuestra dulcísima Madre, la Inmaculada Virgen del Milagro. <b>AMÉN</b>.</p>" +
                titulo1() +
                atributo("CIELO", "Purísima Virgen del Milagro, María, Madre admirable, milagro de la gracia, el primer atributo que simboliza vuestra original pureza, es el Cielo. Influid, Soberana Reina, desde ese hermoso Cielo, con la luz de vuestros auxilios, para que, desengañado mi corazón de la inconstancia de las cosas temporales, sólo busque las eternas y celestiales, considerando que el Cielo es mi patria, para donde fui creado, y que si no aparto mi corazón de lo caduco y terreno, y pongo mi amor en Dios y mi Señor, nunca podré ver el cielo hermoso de vuestro rostro en la gloria. <b>AMÉN</b>.") +
                fin();
    }

    public String dia2() {
        return inicio() +
                tituloDia("SEGUNDO") +
                citas("Colosenses 3, 12-17 / Evangelio según san Lucas 15, 11-32") +
                tituloOracion() +
                "<p>Oh, Dios de mi alma! ¿Qué hubiera sido de mi en aquel momento, si no hubieseis usado de tanta misericordia? Yo estaría en el infierno, donde gimen sin remedio los insensatos cuyas huellas seguí. " +
                "Os doy gracias, Señor, y os ruego que no me abandonéis en mi ceguedad. Digno era de que me hubierais retirado vuestras luces; pero veo que vuestra gracia no me ha abandonado todavía." +
                "Oigo que me llamáis con ternura, me invitáis a conseguir el perdón y a esperarlo todo de Vos, a pesar de las grandes ofensas de que soy culpable en vuestra presencia. "+
                "Sí, oh Salvador mío, espero que me recibiréis por hijo vuestro. No merezco llamarme con tan amoroso nombre, pues tantas veces he osado ultrajados descaradamente: Padre, no soy digno de llamarme hijo tuyo, porque pequé contra el cielo y contra Ti. "+
                "Mas se que vais buscando las ovejas descarriadas, y que os consoláis abrazando a vuestros hijos que andan perdidos. ¡Oh, Padre mío, arrepiéntome de haberos ofendido! Arrójome a vuestros pies, abrazo vuestras rodillas, y no me retiraré hasta que me habréis perdonado y bendecido. Y no os dejaré si no me bendijerais. "+
                "Bendecidme, oh Padre mío, y hágame concebir vuestra bendición un intenso dolor de mis pecados y un ardiente amor para con Vos. Yo os amo, oh Padre mío, os amo con todo mi corazón. "+
                "No permitáis que jamás me separe de Vos. Privadme de todo, pero no me privéis de vuestro amor. Oh, María, si Dios es mi Padre. Vos sois mi Madre. Bendcidme Vos también. "+
                "No merezco ser vuestro hijo, admitidme por vuestro esclavo, pero haced que sea un siervo que os ame tiernamente, y que confíe siempre en vuestra protección. "+
                "Dulcísimo Señor del Milagro, perdonad mis pecados, y librad, por vuestra misericordia, a la ciudad de Salta de todo castigo. "+
                "Concedednos esta gracia, por intercesión de nuestra Protectora, vuestra dulcísima Madre, la Inmaculada Virgen del Milagro. <b>AMÉN</b>."+
                "</p>" +
                titulo1() +
                atributo("SOL", "Purísima Virgen del Milagro, María, Madre admirable, milagro de la gracia, el segundo atributo que simboliza vuestra original pureza, es el Sol. Alcanzadme, Soberana Reina, de vuestro Santísimo Hijo, Sol de justicia, que con los rayos de su divina piedad alumbre las tinieblas en que camina perdida mi alma, para que, conociendo la ceguedad en que he vivido, sepa llorar mis culpas, y al calor de vuestros cariños, se deshagan en raudales mis ojos; pues, siendo Vos mi reina y protectora, me atreví a ofendemos y a despreciar vuestra gloria, para que, purificada mi alma con la contrición de mis culpas, merezca ver en la gloria, el verdadero Sol de Justicia que nació de Vos. <b>AMÉN</b>") +
                fin();
    }

    public String dia3() {
        return inicio() +
                tituloDia("TERCERO") +
                citas("Romanos 13, 8-14 / Evangelio según san Lucas 7, 36-50 / 1ª Carta del apóstol san Juan") +
                tituloOracion() +
                "<p>" +
                "Oh, Jesús y Redentor mío! Gracias os doy de que no hayáis permitido que muriese cuando estaba en desgracia vuestra. " +
                "¡Cuántos años seguidos no merecía yo estar sepultado en el abismo del infierno! Si yo hubiese muerto tal día, aquella noche, ¡qué hubiera sido de mí por toda una eternidad Señor, gracias os doy mil veces por tal beneficio. " +
                "Yo acepto la muerte en satisfacción de mis pecados; y la acepto tal cual sea de vuestro agrado enviármela; mas ya que me la habéis retardado hasta el presente, retardadla aún, Dios, mío, dejadme pues, que llore un poquito mi dolor. "+
                "Dadme tiempo para llorar las ofensas de que me hice culpable a vuestros ojos, antes que llegue el día en que habéis de juzgarme. "+
                "No quiero ya resistir por más tiempo a vuestra voz. ¡Quién sabe si las palabras que acabo de oír son el último clamor que me hacéis escuchar! Confieso que soy indigno de misericordia. "+
                "Tantas veces me habéis perdonado, y yo ingrato os he ofendido de nuevo. Al corazón contrito y humillado no lo despreciarás, o Dios. "+
                "Señor, ya que no desecháis un corazón que se arrepiente y se humilla, ved ahí al traidor que vuelve a Vos herido por la flecha del arrepentimiento. No me deseches de tu rostro. Por piedad, no me arrojéis de vuestra presencia. "+
                "Vos mismo dijisteis: Aquel que a Mí viene, no le echaré fuera. Verdad es que más que nadie os he ultrajado, porque más que a nadie me habéis favorecido con vuestras luces y con vuestras gracias; pero la sangre que por mí habéis derramado, me da aliento, y me hace esperar el perdón, si de veras me arrepiento - Sí, oh mi Soberano bien, yo me arrepiento con toda mi alma de haberos despreciado. "+
                "Perdonadme, y concededme la gracia de amaros en adelante. Harto estoy ya de haberos ofendido. " +
                "El tiempo que me queda para vivir, oh dulce Jesús mío, no quiero emplearlo más en ofendemos; quiero tan sólo llorar amargamente por los disgustos que he podido daros. "+
                "Amaros quiero con toda la fuerza de mi alma. ¡Oh, Dios, que merecéis un amor infinito! ¡Oh, María, mi esperanza, rogad a Jesús por mí!<br>" +
                "Dulcísimo Señor del Milagro, perdonad mis pecados, y librad, por vuestra misericordia, a la ciudad de Salta de todo castigo. "+
                "Concedednos esta gracia, por intercesión de nuestra Protectora, vuestra dulcísima Madre, la Inmaculada Virgen del Milagro. " +
                "<b>AMÉN</b>."+
                "</p>" +
                titulo1() +
                atributo("ESTRELLA", "Purísima Virgen del Milagro, María Madre admirable, milagro de la gracia, el tercer atributo que simboliza vuestra original pureza, es la Estrella de Jacob. Y pues sois Estrella resplandeciente que en la oscura noche de esta vida, alumbráis con vuestras luces a los que perdidos caminan, ya veis, piadosísima Reina y Estrella de pecadores, el camino que llevan mis pasos; influid con vuestras benignas influencias, para que yo camine seguro por el camino verdadero que conduce a la gloria, que es el de la cruz y mortificación, para que, viviendo crucificado, al mundo y a mis pasiones, merezca por vuestra intercesión ser estrella resplandeciente en la gloria. <b>AMÉN</b>") +
                fin();
    }

    public String dia4() {
        return inicio() +
                tituloDia("CUARTO") +
                citas("Carta a los Filipenses 2, 3-11 / Evangelio según san Juan 3, 16-21") +
                tituloOracion() +
                "<p>" +
                "Oh, Jesús mío, pues quiero siempre llamaros por vuestro nombre; esto me consuela y me da valor, cuando me acuerdo de que sois mi Salvador, y que habéis muerto para salvarme. " +
                "Vedme a vuestros pies; confieso que soy digno de tantos infiernos, como veces os he ofendido por el pecado mortal. "+
                "No merezco perdón; pero Vos moristeis para perdonarme. Piadoso Jesús, no olvides que por mi fue tu venida. "+
                "Anticipaos, oh Jesús mío, a perdonarme antes que vengáis a juzgarme. Entonces yo no podré pediros piedad; más ahora puedo, y espero que me la concederéis. "+
                "Entonces vuestras llagas me llenarán de espanto, ahora me inspiran confianza. ¡Oh, Redentor de mi alma!, yo me arrepiento sobre todo de haber ofendido a vuestra infinita bondad, y prefiero sufrir todas las pérdidas posibles, antes que burlarme de vuestra gracia. "+
                "Os amo con todo mi corazón, tened piedad de mí. Ten piedad de mi oh Dios, según tu grande misericordia. "+
                "Oh, María, Madre de misericordia, abogada de los pecadores, alcanzadme un intenso dolor de mis pecados, el perdón y la perseverancia en el divino amor. "+
                "Yo os amo, Reina de mi corazón, y en Vos pongo toda mi confianza.<br>"+
                "Dulcísimo Señor del Milagro, perdonad mis pecados, y librad, por vuestra misericordia, a la ciudad de Salta de todo castigo. "+
                "Concedednos esta gracia, por intercesión de nuestra Protectora, vuestra dulcísima Madre, la Inmaculada Virgen del Milagro. " +
                "<b>AMÉN</b>."+
                "</p>" +
                titulo1() +
                atributo("ARCA DEL TESTAMENTO", "Purísima Virgen del Milagro, María, Madre admirable, milagro de la gracia, el cuarto atributo que simboliza vuestra original pureza, es el Arca del Testamento. Y Pues sois Arca divina que, para que no pereciésemos en el diluvio de nuestras culpas bajasteis a las aras del altar para asegurarnos en Jesús Sacramentado, concededme, Madre mía, el que, no hallando descanso en este mundo sino en Jesús Sacramentado, se aquieten nuestras potencias y sentidos, para que, gustando las dulzuras de este Pan Soberano, sienta aún en esta vida las delicias y gozos, que dan a los que os sirven en la bienaventuranza de la gloria. <b>AMÉN</b>") +
                fin();
    }

    public String dia5() {
        return inicio() +
                tituloDia("QUINTO") +
                citas("Romanos 8. 35-39 / Evangelio según san Mateo 22, 34-40 / 1ª Carta del apóstol san Juan 4, 7-21") +
                tituloOracion() +
                "<p>" +
               "Oh, Dios mío, conque Vos sois mi soberano bien, bien infinito y yo os he perdido tantas veces! ¡Sabía que por el pecado os causaría el mayor disgusto, pues perdería vuestra gracia, y a pesar de esto yo lo cometía! ¡Ah, si yo no os " +
                "viese clavado en una cruz, oh Hijo de Dios, y muriendo por mí, no me atrevería a invocaras, ni a esperar jamás el perdón! ¡Padre Eterno, no fijéis en mí vuestros ojos, sino en vuestro Hijo querido, que os está clamando por mí, " +
                "misericordia: escuchadle y perdonadme! Muchos años hace que debiera hallarme sepultado en el infierno, y sin esperanza de amaros y de recobrar la gracia que he perdido. "+
                "¡Oh, Dios! arrepiéntome del ultraje que os hice renunciando a vuestra amistad, y despreciando vuestro amor por los miserables placeres de este mundo. "+
                "¡Ah pluguiera a Dios que hubiese muerto mil veces antes que ofenderos! ¿Cómo pudo llegar a tal extremo mi ceguedad y locura? Agradezco, oh Dios mío, de haberme dado tiempo para poder pensar en el mal que he hecho, y ya que "+
                "por un efecto de vuestra misericordia no me hallo en el infierno y puedo amaros y amaros quiero, oh Dios mío, y no quiero diferir un solo instante el convertirme a Vos. "+
                "Os amo, bondad infinita; os amo, oh vida mía, mi tesoro, mi amor, mi todo. Recordadme siempre el amor que me habéis tenido y el infierno donde debía encontrarme, a fin de que esta idea me inflame y me obligue a hacer actos de "+
                "amor y a deciros siempre: Yo os amo. "+
                "¡Oh, María, Reina de mi corazón, esperanza mía, Madre mía, si me hallara en el infierno, no podría amaros jamás! Yo os amo, oh Madre mía; en Vos pongo toda mi confianza, y espero no abandonamos ya más, ni a Vos, ni a mi Dios. "+
                "Socorredme y rogad por mí a Jesús.<br>"+
                "Dulcísimo Señor del Milagro, perdonad mis pecados, y librad, por vuestra misericordia, a la ciudad de Salta de todo castigo. "+
                "Concedednos esta gracia, por intercesión de nuestra Protectora, vuestra Dulcísima Madre, la Inmaculada Virgen del Milagro."+
                "<b>AMÉN</b>."+
                "</p>" +
                titulo1() +
                atributo("PALOMA", "Purísima Virgen del Milagro, María, Madre admirable, milagro de la gracia, el quinto atributo que simboliza vuestra original pureza, es la Paloma, que, volando a nuestra tierra, trajisteis el ramo de olivo para asegurar a los suyos que habían cesado ya por vuestra intercesión, las aguas de las tribulaciones. Concededme, Madre mía, el que, cual paloma que gime la pérdida de su consorte, así sepa yo llorar y sentir las muchas culpas con que he perdido a mi dulce Jesús, Esposo de mi alma, y que agradecido lave con mis lágrimas, las manchas con que he afeado mi alma, para que, vestida con la candidez de vuestra gracia, vuele en compañía de vuestro Esposo, el Espíritu Divino, a alabaros en la Gloria. <b>AMÉN</b>") +
                fin();
    }

    public String dia6() {
        return inicio() +
                tituloDia("SEXTO") +
                citas("Gálatas 6, 7-18 / Evangelio según san Lucas 12, 39-46 / Evangelio según san Lucas  23, 39-43") +
                tituloOracion() +
                "<p>" +
                "Oh, mi Jesús! ¿Cómo habéis podido sufrirme tanto tiempo? ¡Tantas veces como he huido de Vos, y a pesar de esto Vos habéis venido siempre a mi encuentro! ¡Tantas veces como os he ofendido, y Vos me habéis perdonado! ¡Os ofendí de nuevo, y de nuevo me habéis concedido el perdón! ¡Ah, hacedme sentir un poco de aquel dolor que padecisteis en el Huerto de Getsemaní, cuando, al pensar en nuestros pecados, llegasteis a sudar sangre. "+
                "Yo me arrepiento, oh Redentor mío, de haber tan mal correspondido, a vuestro amor. ¡Oh placeres malditos, yo os detesto y abomino! Vosotros me hicisteis perder la gracia del Señor. "+
                "¡Oh, amado Jesús!, yo os amo sobre todas las cosas, y renuncio a todas las satisfacciones ilícitas, prefiriendo morir mil veces antes que ofendemos jamás. "+
                "¡Ah! por el afecto que me mostrasteis en la Cruz, y que os obligó a ofrecer por mí esa vida divina, dadme la luz y la fuerza para resistir las tentaciones, y recurrir en ellas a vuestra ayuda. "+
                "¡Oh, María, mi esperanza, ya que todo lo podéis con Dios, alcanzadme la santa perseverancia, y haced que no me separe jamás de vuestro amor!<br>"+
                "Dulcísimo Señor del Milagro, perdonad mis pecados, y librad, por vuestra misericordia, a la ciudad de Salta de todo castigo. "+
                "Concedednos esta gracia, por intercesión de nuestra Protectora, vuestra dulcísima Madre, la Inmaculada Virgen del Milagro."+
                "<b>AMÉN</b>."+
                "</p>" +
                titulo1() +
                atributo("JARDÍN CERRADO", "Purísima Virgen del Milagro, María, Madre admirable, milagro de la gracia, el sexto atributo que simboliza vuestra original pureza, es el Jardín Cerrado. Encerrad, Madre mía, en vuestro corazón purísimo, como en jardín soberano, todos nuestros pensamientos y obras, para que de hoy en adelante ya no piense en otra cosa, sino sólo en serviros, ni quiera más gloria que la de amaros, y haced que con la fragancia suavísima de todas vuestras virtudes se conviertan nuestros deseos en fruto de vuestro agrado, para que, adornándose mi alma con las flores de las virtudes, merezca ser suave olor de Cristo en la gloria. <b>AMÉN</b>") +
                fin();
    }

    public String dia7() {
        return inicio() +
                tituloDia("SEPTIMO") +
                citas("1 Jn 3, 14-18 / Evangelio según san Juan 15, 9-17") +
                tituloOracion() +
                "<p>" +
                "Mirad, oh mi Dios, mirad a vuestros pies un ingrato a quien creasteis para el paraíso, pero que tantas veces, por miserables placeres, os ha negado a la cara y ha preferido ser condenado al infierno. "+
                "Más yo espero, que Vos me, habéis perdonado todas las injurias que os he hecho, de las cuales me arrepiento de nuevo y quiero arrepentirme hasta la muerte. "+
                "¡Ah, yo deseo que me las perdonéis todavía! Más, oh Dios mío, aunque Vos me hayáis perdonado, no por esto será menos verdad que tuve la audacia de abrevaros de amargura, oh Redentor mío, que para conducirme a vuestro Reino me habéis dado la vida. "+
                "¡Bendita y glorificada sea para siempre, oh mi Jesús, vuestra misericordia. Vos que con tanta paciencia me habéis sufrido, y que, en vez de castigarme me habéis colmado de gracia y de luces, y mil veces me habéis llamado a Vos, ya veo, oh mi amado Jesús, que Vos queréis que me salve, deseáis que entre a vuestro Reino para amaros eternamente; pero antes queréis que os ame en este mundo. "+
                "Sí, yo quiero amaros; y aún cuando no hubiere paraíso, en tanto que yo viva, os amaré con todas mis fuerzas y con toda mi alma. Bástame saber, oh mi Dios, que Vos deseáis que os ame. "+
                "¡Oh, mi Jesús!, asistidme con vuestra gracia, y no me abandonéis. Mi alma es inmortal: hállome, pues, en la alternativa, o de amaros siempre o de detestaros por toda una eternidad. "+
                "¡Ah, no, no!, amaros quiero eternamente, y amaros lo bastante en esta vida, para amaros en la otra lo que debo. Disponed de mí como os plazca; castigadme como Vos quisierais, pero no me privéis de vuestro amor; haced después, de mí, lo que os parezca., ¡Oh mi Jesús! vuestros méritos son mi esperanza ¡Oh, María, toda mi esperanza la pongo en vuestra intercesión! Vos me habéis librado del infierno, cuando yo estaba en pecado. "+
                "Ahora quiero ser de Dios; hacedme santo y salvadme.<br>"+
                "Dulcísimo Señor del Milagro, perdonad mis pecados, y librad, por vuestra misericordia, a la ciudad de Salta de todo castigo. "+
                "Concedednos esta gracia, por intercesión de nuestra Protectora, vuestra dulcísima Madre, la Inmaculada virgen del Milagro. "+
                "<b>AMÉN</b>."+
                "</p>" +
                titulo1() +
                atributo("PUERTA DEL CIELO", "Purísima Virgen del Milagro, María, Madre admirable, milagro de la gracia, el séptimo atributo que simboliza vuestra original pureza, es la Puerta del Cielo. Y pues sois Puerta Celestial, por cuya intercesión entran al paraíso de la gloria, los hijos de Adán que acaban esta vida en gracia; concededme, Madre mía, el que si este año fuese el último de mi vida, se aparte mi corazón de los cuidados de este mundo, y con la luz de vuestros auxilios busque sólo el sosiego de mi alma en mi dulce Jesús, para que cuando llegue la muerte temporal, merezca estar dispuesto para acabar mi vida en el ósculo de mi Señor, y entre por Vos, Puerta resplandeciente, a la patria dichosa de la gloria. <b>AMÉN</b>") +
                fin();
    }

    public String dia8() {
        return inicio() +
                tituloDia("OCTAVO") +
                citas("Hebreos 9, 11-15 / Hebreos 10, 19-31 / Evangelio según san Juan 6, 35-46") +
                tituloOracion() +
                "<p>" +
                "Oh, Bien supremo, yo soy este miserable que he huido de Vos, renunciando a vuestro amor! Por esto sólo, indigno debiera ser de veros y de amaros. "+
                "Más también Vos sois Aquel que por piedad de mí no la tuvisteis de Vos mismo, y quisisteis morir de dolor y cubierto de infamia en una Cruz. "+
                "Vuestra muerte me hace, pues, esperar que un día podré yo veros y gozar de vuestra presencia, amándoos con todas mis fuerzas. Más ahora que estoy en continuo peligro de perderos para siempre, y que ya os había perdido por mis pecados, ¿qué haré durante el resto de mi vida? ¿Continuaré en ofendemos? No, Jesús mío, yo detesto sobremanera los ultrajes que os he hecho, contrito estoy de haberos ofendido, y os amo de todo corazón. "+
                "¿Desecharíais Vos un alma que se arrepiente y que os ama?. No. Yo sé que habéis dicho, Redentor mío, que no sabéis rechazar a los que se arrojan a vuestros pies arrepentidos: Aquel que a mí viene, no le echaré fuera. "+
                "¡Oh, Jesús mío, todo lo abandono y me convierto a Vos!. Os abrazo y os estrecho contra mi corazón; dignaos Vos abrazarme y estrecharme en el vuestro. "+
                "Si me atrevo a hablaros así, es porque me dirijo a la bondad infinita, y por que hablo a un Dios que ha querido morir por mi amor. ¡Oh, Salvador mío, dadme la esperanza en vuestro amor!. "+
                "¡Oh María, querida Madre mía os lo suplico por el amor que tenéis a Jesucristo, alcanzadme la perseverancia! Así lo espero y así sea.<br>"+
                "Dulcísimo Señor del Milagro, perdonad mis pecados y librad, por vuestra misericordia, a la ciudad de Salta de todo castigo. "+
                "Concedednos esta gracia, por intercesión de nuestra Protectora, vuestra dulcísima Madre, la Inmaculada Virgen del Milagro. "+
                "<b>AMÉN</b>."+
                "</p>" +
                titulo1() +
                atributo("FUENTE DE AGUAS VIVAS", "Purísima Virgen del Milagro, María, Madre admirable, milagro de la gracia, el octavo atributo que simboliza vuestra original pureza, es el ser Fuente de Aguas. Concededme, Madre mía, el que, cual sediento ciervo que busca las aguas, corra yo a beber de aquellas cinco fuentes que por mí derramó mi dulce Jesús en el madero santo de la Cruz, para que, atraído de las dulzuras que comunican aquellas santísimas llagas, lave yo en aquellas purísimas aguas las muchas manchas con que he afeado mi alma, para que, cuando venga mi Señor a juzgarme y aparezca en el Cielo aquel madero santo de la Cruz, llore lágrimas dé consuelo al ver que, aunque desprecié las fuentes de aguas vivas, la Cruz fue la llave que me abrió las puertas de la gloria. <b>AMÉN</b>") +
                fin();
    }

    public String dia9() {
        return inicio() +
                tituloDia("NOVENO") +
                citas("Efesios 1, 3-14 / Evangelio según san Juan 10, 10-18 / Evangelio según san Juan 17, 20-26") +
                tituloOracion() +
                "<p>" +
                "Gracias os doy, oh dulcísimo Redentor mío, por estas luces que me acabáis de dar, y por haberme dado a conocer los medios para salvarme. " +
                "Yo os prometo ponerlos en práctica con la mayor firmeza. Concededme la gracia que necesito para seros fiel; ya veo ser vuestra voluntad el que yo me salve, y yo quiero salvarme, en especial para dar gusto a vuestro Corazón divino, que con tanto ardor desea mi salvación. " +
                "No, no quiero resistir, oh Dios mío, por más tiempo al amor que me tenéis. Este amor ha sido causa para que Vos me hayáis sufrido con tanta paciencia, cuando yo os ofendía, Vos me llamáis a vuestro amor, y yo no deseo sino amaros. " +
                "Os amo, bondad infinita; os amo, oh bien supremo e infinito; y os suplico hoy día, por los méritos de Jesucristo, no permitáis que yo sea jamás ingrato a vuestra bondad. " +
                "Haced, que cese de ser ingrato, o poned fin a mi vida. Señor, ya que habéis empezado la obra, dignaos ahora completarla. " +
                "Confirma oh Dios, lo que has hecho en nosotros. Dadme la luz, la fuerza, dadme el amor. " +
                "¡Oh María, Vos que sois la dispensadora de las gracias, socorredme! Admitidme por vuestro servidor, pues quiero serlo, y rogad a Jesús por mí. " +
                "Los méritos de Jesucristo, y después vuestros ruegos, son los que han de salvarme.<br>" +
                "Dulcísimo Señor del Milagro, perdonad mis pecados, y librad, por vuestra misericordia a la ciudad de Salta de todo castigo. " +
                "Concedednos esta gracia, por intercesión de nuestra Protectora, vuestra dulcísima Madre, la Inmaculada Virgen del Milagro. "+
                "<b>AMÉN</b>."+
                "</p>" +
                titulo1() +
                atributo("TRONO", "Purísima Virgen del Milagro, María, Madre admirable, milagro de la gracia, el noveno atributo que simboliza vuestra original pureza, es el ser Trono de Dios. Pues sois Trono en quien descansó el Señor, como en trono de toda santidad y perfección, concededme, Madre mía, el que, ya que mi corazón camina perturbado con los engaños de este mundo, descanse sólo en Vos, y sienta aquel sosiego y alegría que experimentan vuestros siervos; y pues, nuestro amor os obligó a dejar vuestro trono y bajar a pedir que el Señor suspendiese el castigo contra el pueblo de Salta, os suplico, Madre mía del Milagro, continúe vuestra piedad y misericordia, ya suspenda los castigos que cada día merezco por mis culpas. Y si, como frágil y miserable, me olvidase algún día de vuestro amor, Vos, como que sois todo nuestro consuelo y amparo, dadme silbos amorosos para que yo vuelva cual descarriada oveja al rebaño de mi Señor, y por vuestra intercesión merezca veros en el trono de la gloria. <b>AMÉN</b>") +
                fin();
    }

    private String fin() {
        return titulo2() +
                oracion3() +
                oracion4() +
                doceEstrellasDelCieloDeMaria();
    }

    private String inicio(){
        return actoContriccion() +
                oracionTodosLosDias() ;
    }

    private String tituloOracion() {
        return "<p style=\"text-align:center\"><b>ORACIÓN</b><br><p>";
    }

    private String citas(String cita) {
        return "<p style=\"text-align:center\">" + cita + "</p>";
    }

    private String tituloDia(String dia) {
        return "<p style=\"text-align:center\"><b>DÍA " + dia + "</b><br></p>";
    }

    private String atributo(String atributo, String oracion) {
        return "<p style=\"text-align:center\"><b>" + atributo + "</b><p>" +
                "<p>" + oracion + "</p>";
    }

    private String titulo1() {
        return "<p style=\"text-align:center\"><b>ATRIBUTOS DE MARÍA</b><p>";
    }

    private String titulo2() {
        return "<p style=\"font-style:oblique\">Se pedirá lo que se deseare conseguir.</p>";
    }

    private String actoContriccion() {
        return "<p style=\"text-align:center\"><b>ACTO DE CONTRICIÓN (1)</b></p>" +
                "<p>" +
                "Dulce Jesús mío y mi crucificado Señor, indigno de ponerme delante de vuestros ojos, me postro avergonzado a vuestros pies, confesando la multitud de mis culpas, con íntimo dolor de mi alma, por haber sido ofensa contra Vos. " +
                "Herido vengo, médico divino, a buscar mi remedio en vuestra benigna misericordia, proponiendo con todo mi corazón la enmienda. " +
                "Dulce amor mío sois sobre todas las cosas, tened piedad de mí, y acordaos, Señor, el que mi amor os puso en esa Cruz, y no os acordéis el que yo, como ingrato y desconocido, me olvidé de vuestro paternal amor, porque si a Vos, que " +
                "sois mi Padre, no vuelvo los ojos, ¿quién otro se compadecerá de mí? ¡Ay, mi Jesús, y cómo os ofendí! ¡Oh, quién de dolor muriera a vuestros pies, pues amándome tanto me atreví a ofender a un Dios tan bueno, tan santo y tan amable! " +
                "Pequé, Padre mío, contra el cielo y contra Vos, tened misericordia de mí. <b>AMÉN</b>." +
                "</p>";
    }

    private String oracionTodosLosDias() {
        return "<p style=\"text-align:center\"><b>ORACIÓN PREPARATORIA PARA TODOS LOS DÍAS (2)</b></p>" +
                "<p>María Purísima del Milagro, que con tierno amor te inclinaste a pedir a tu Soberano Hijo, cuando enojado por nuestras culpas, quiso destruir la ciudad de Salta con aquellos espantosos terremotos, y Tú, cual otra hermosa Ester, puesta " +
                "delante del Supremo Rey de los Cielos, mudando de colores, pediste por la libertad de este pueblo; concédeme, Madre mía del Milagro, el que de tal suerte mude yo mi vida, que si hasta aquí he caminado por los caminos de mi " +
                "perdición, olvidado de mi Dios y Señor, de hoy en adelante sólo reine en mi corazón tu maternal amor, y que corresponda, amante y agradecido, a las obligaciones de hijo de tal Madre. " +
                "Y no permitas Madre mía, el que se vea malograda en mí tu poderosa intercesión, que todo lo puede conseguir, con tal que no apartes tus purísimos ojos de este miserable pecador y concédeme lo que te pido en esta novena, si es para " +
                "mayor honra y gloria tuya, y bien de mi alma. <b>AMÉN</b>. " +
                "</p>" +
                "<p style=\"font-style:oblique\">Aquí se rezan tres Avemarías en reverencia de la Purísima Concepción.</p>";
    }

    private String oracion3() {
        return "<p style=\"text-align:center\"><b>ORACIÓN (3)</b><p>" +
                "<p>Soberana Emperatriz de los Cielos y tierra, dulcísima Madre de pecadores, Madre del Milagro, en ésta tu escogida ciudad en la cual ostentas tu amor, mírame con semblante risueño, que, aunque pecador y desagradecido, soy hijo " +
                "tuyo, y te venero y amo como a Madre amorosa y admirable. " +
                "Y creo que si en mí empleas tus purísimos ojos, no me ha de desamparar mi Señor Jesucristo, porque a los que Tú tienes bajo tu patrocinio, les muestra El especial amparo. " +
                "Ea, pues, Madre mía del Milagro, no desprecies mis ruegos, y si cuando como pecador no te busqué, Tú solicitabas mi amistad porque deseabas mi salvación, ¿cómo ahora, que con tanta ansia te busco, me has de negar tu amparo, " +
                "tu patrocinio y favor? Merezca yo tu poderoso brazo, ahora que arrodillado te pido me lleves de la mano a tu amado Hijo crucificado, para que, viendo mi dolor y arrepentimiento de mis culpas y pecados, que deseo sea mayor que el " +
                "me han tenido los más penitentes Santos del mundo, me lleve a sí y me dé a beber de aquella Sangre Soberana de su amoroso Costado, que es todo el precio de nuestra redención, y viva sólo en El, huyendo del mundo y de mí mismo. <b>AMÉN</b>." +
                "</p>" +
                "<p style=\"font-style:oblique\">Se reza un Credo a Cristo Crucificado.</p>";
    }

    private String oracion4() {
        return "<p style=\"text-align:center\"><b>ORACIÓN (4)</b></p>" +
                "<p>" +
                "Amantísimo Jesús mío, hermosura eterna de la Gloria, mi Dios crucificado y todo mi bien, justo Juez y piadoso Padre, que, no contento vuestro amor con haber bajado del cielo a la tierra a buscar al pecador; haber derramado vuestra " +
                "sangre en el ara de la Cruz y haber instituido el Sacramento Eucarístico de vuestro Cuerpo y Sangre en la Santa Misa, quisisteis venir en vuestra milagrosa imagen a esta ciudad de Salta, a buscar como Pastor Divino a la oveja perdida. " +
                "Y cuando más olvidada andaba de vuestro singular amor, hicisteis estremecer la tierra con espantosos terremotos, y revelasteis a vuestro siervo no cesarían hasta que os sacasen por las calles. " +
                "Os suplico, mi Dios crucificado, que por vuestra mansedumbre soseguéis la inquietud de mi espíritu, para que pueda yo corresponder agradecido, buscándoos sólo a Vos, pues sois el descanso de mi alma y mi único bien. " +
                "Y si por haberos ofendido temblase mi alma de llegarse a Vos, dadle voces desde esa Cruz, con que interiormente le digáis: \"Mira, hijo mío, cuánto sufro por tu amor, y tú ¿qué es lo que haces por Mí, sino sólo ofenderme? Pero ven a " +
                "mis brazos, que Yo clamaré a mi Eterno Padre diciendo: \"Padre, perdona a este hijo ingrato, que no ha sabido lo que ha hecho al haber despreciado a su Dios y Redentor\" Y, si todavía vuestro amor retira de mi los ojos de su piedad, " +
                "por mi ignorancia e ingratitud, ponedlos en vuestra Madre, María Santísima del Milagro, mi Protectora, por cuyos méritos y piadosa intercesión, espero se templarán vuestros enojos, y me daréis gracia para que os pueda servir en esta " +
                "vida y alabaros en la eterna. <b>AMÉN</b>." +
                "</p>";
    }

    private String doceEstrellasDelCieloDeMaria() {
        return "<p style=\"text-align:center\"><b>DOCE ESTRELLAS DEL CIELO DE MARÍA</b></p>" +
                "<p style=\"text-align:center\">I.<br>" +
                "Dios te salve, Madre<br>" +
                "Reina de los Cielos,<br>" +
                "esperanza nuestra<br>" +
                "refugio y consuelo.<br>" +
                "<br>" +
                "II.<br>" +
                "Virgen del Milagro,<br>" +
                "gloria de este pueblo,<br>" +
                "en quien siempre halla<br>" +
                "todo su remedio.<br>" +
                "<br>" +
                "III.<br>" +
                "Si son nuestras culpas<br>" +
                "muchas en extremo<br>" +
                "tus misericordias<br>" +
                "son más con exceso.<br>" +
                "<br>" +
                "IV.<br>" +
                "Ya el castigo estaba<br>" +
                "sobre nuestros yerros,<br>" +
                "más lo detuvieron<br>" +
                "tus piadosos ruegos.<br>" +
                "<br>" +
                "V.<br>" +
                "Al pie del sagrario<br>" +
                "allí intercediendo,<br>" +
                "el perdón pediste<br>" +
                "de nuestros excesos.<br>" +
                "<br>" +
                "VI.<br>" +
                "Mudando colores<br>" +
                "tu semblante bello<br>" +
                "a entender nos dio<br>" +
                "tu pena y consuelo.<br>" +
                "<br>" +
                "VII.<br>Empeñada estabas<br>" +
                "y echaste Tú el resto,<br>" +
                "para que el castigo<br>" +
                "no tuviese efecto.<br>" +
                "<br>" +
                "VIII.<br>" +
                "Perdona –decías–<br>" +
                "mi Dios a este pueblo,<br>" +
                "si no la corona<br>" +
                "de Reina aquí os dejo.<br>" +
                "<br>" +
                "IX.<br>" +
                "Yo por fiadora<br>" +
                "salgo es este empeño,<br>" +
                "y a mi cuenta corre<br>" +
                "no más ofenderos<br>" +
                "<br>" +
                "X.<br>" +
                "Confundirte quiso<br>" +
                "el dragón soberbio,<br>" +
                "pero con tu planta<br>" +
                "le quebraste el cuello.<br>" +
                "<br>" +
                "XI.<br>" +
                "Haz, Madre y Señora,<br>" +
                "que todos logremos<br>" +
                "el fruto, después<br>" +
                "de aqueste destierro.<br>" +
                "<br>" +
                "XII.<br>" +
                "En esta novena<br>" +
                "que humilde hacemos,<br>" +
                "nuestra petición<br>" +
                "por tu amor logremos.<br>";
    }
}
