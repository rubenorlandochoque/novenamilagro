package com.umayuxsolutions.novenamilagro.texts;

public class Pacto {
    public String pacto() {
        return
                "<p style=\"text-align:center\"><b>Pacto de Fidelidad</b><br></p>" +
                        "<p style=\"text-align:center\"><b>Al Señor del Milagro</b><br></p>" +
                        "<p>" +
                        "Divino Jesús Crucificado y Señor Nuestro del Milagro, el pueblo de Salta hoy postrado en vuestra presencia, viene a renovar los votos de sus mayores, cuando acudiera a Vos, encontrando remedio en su aflicción. " +
                        "Sí, ante esa Cruz que nos enviasteis a través de los mares para ser nuestro escudo y defensa, juramos, lo que juraron nuestros padres, teneros siempre por Padre, Abogado y Patrono, y reconocer vuestra real soberanía sobre todos los pueblos, y especialmente sobre el nuestro.<br>" +
                        "Confesamos que sois el camino, la verdad y la vida, así de los individuos como de las familias, pueblos y naciones; y que lejos de Vos y de los esplendores de vuestra Cruz solo se encuentran engaños y amarguras.<br>" +
                        "Hacernos nuestro el pacto de fidelidad celebrado por nuestros antepasados, <b>PROMETIENDO QUE VOS, DULCE JESUS, SERAS SIEMPRE NUESTRO, Y QUE NOSOTROS SEREMOS SIEMPRE TUYOS</b>. " +
                        "Extiéndanse vuestros brazos sobre este pueblo y la Nación Argentina, para protegernos y defendernos, y haced que las verdades de nuestra fe y enseñanzas de la Iglesia, sean siempre el norte de nuestras acciones y el fundamento inconmovible de nuestras instituciones. ¡Señor del Milagro, salvad y, bendecid nuestro pueblo!. <b>AMÉN</b>." +
                        "</p>"+
                        "<p style=\"text-align:center\"><b>A la Virgen del Milagro</b><br></p>" +
                        "Virgen Inmaculada, Madre y Señora Nuestra del Milagro, el pueblo de Salta postrado a vuestros pies, quiere reconocer y renovar los votos de sus padres, al jurar vuestro patronato y ponerse bajo vuestra protección. "+
                        "Sí, en presencia del cielo y de la tierra, hacemos nuestro el voto que en Septiembre de 1692 hiciera este pueblo, de celebrar los días en que os manifestasteis su especial Protectora, y juramos teneros siempre por Madre y Abogada nuestra. "+
                        "Y Vos Señora, dignaos bendecir y proteger este pueblo mirándolo como heredad vuestra, para que sea siempre fiel a la fe, a las enseñanzas de la Iglesia y a los compromisos contraídos. Nuestra Señora del Milagro, rogad por nosotros. <b>AMÉN</b>. "+
                        "<p>" +
                        "</p>";
    }
}
