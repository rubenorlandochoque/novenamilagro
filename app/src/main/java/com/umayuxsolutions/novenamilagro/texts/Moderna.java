package com.umayuxsolutions.novenamilagro.texts;

public class Moderna {
    public String dia1() {
        return inicio() +
                tituloDia("PRIMERO") +
                citas("Filipenses  3,  17  -  4,3  /  Evangelio  según  san  Mateo  7,7-10  /  Corintios  4,13-18  / Evangelio según san Lucas 11,5-13") +
                tituloOracion() +
                "<p>Tú eres, Dios, el Bien infinito, y más de una vez te he cambiado por un egoísta placer del momento. Más, aún cuando te haya  despreciado, me  ofreces todavía el perdón, si yo lo quiero y prometes recibirme en tu gracia, si me arrepiento de haberte ofendido. Sí, Señor, me arrepiento de todo corazón de haberte negado tan mezquinamente; aborrezco mi pecado más que todo otro mal. Y ahora, al volver a Ti, " +
                "espero  me  recibirás  y  me  abrazarás  como  amoroso  Padre.  Te  agradezco,  infinita " +
                "Bondad, y necesito tu auxilio.<br>" +
                "No me lo niegues, Dios mío, y no permitas que me separe jamás de Ti. No dejará de tentarme el infierno, pero Tú eres más fuerte que el infierno. Sé que si siempre a Ti me encomiendo, jamás me separaré de Ti, y esta es la única gracia que te pido: haz que nunca cese de rogarte como ahora lo hago. Asísteme Señor, dame la luz, la fuerza, la perseverancia, dame el paraíso. Sobre todo dame tu amor, que es el paraíso de las almas. Te amo, bondad infinita, y quiero amarte siempre; escúchame, por amor a Jesucristo. " +
                "María, Tú que eres el refugio de los pecadores, socorre a uno que quiere amar sinceramente a nuestro Dios.<br>" +
                "Dulcísimo Señor del Milagro, perdona mis pecados, y libra, por tu misericordia, al pueblo de Salta y a tus devotos de todo castigo. Concédenos esta gracia, por intercesión  de  nuestra  Protectora,  tu  dulcísima  Madre,  la  Inmaculada  Virgen  del Milagro. <b>AMÉN</b>.<br><p>" +

                titulo1() +
                atributo("CIELO", "Purísima Virgen del Milagro, María, Madre admirable, milagro de la gracia; el primer atributo que simboliza tu original pureza, es el Cielo. Influye, Soberana Reina, desde ese hermoso Cielo, con la luz de tus auxilios; para que, desengañado mi corazón de la inconstancia de las cosas temporales, sólo busque las eternas y celestiales. Hazme considerar que el Cielo es mi patria, para la que fui creado, y que si no aparto mi corazón de lo caduco y terreno, poniendo mi amor en Dios y mi Señor, nunca podré ver el cielo hermoso de tu rostro en la gloria. <b>AMÉN</b>") +
                fin();
    }

    public String dia2() {
        return inicio() +
                tituloDia("SEGUNDO") +
                citas("Filipenses  3,  17  -  4,3  /  Evangelio  según  san  Mateo  7,7-10  /  Corintios  4,13-18  / Evangelio según san Lucas 11,5-13") +
                tituloOracion() +
                "<p>Tú eres, Dios, el Bien infinito, y más de una vez te he cambiado por un egoísta placer del momento. Más, aún cuando te haya  despreciado, me  ofreces todavía el perdón, si yo lo quiero y prometes recibirme en tu gracia, si me arrepiento de haberte ofendido. Sí, Señor, me arrepiento de todo corazón de haberte negado tan mezquinamente; aborrezco mi pecado más que todo otro mal. Y ahora, al volver a Ti, " +
                "espero  me  recibirás  y  me  abrazarás  como  amoroso  Padre.  Te  agradezco,  infinita " +
                "Bondad, y necesito tu auxilio.<br>" +
                "No me lo niegues, Dios mío, y no permitas que me separe jamás de Ti. No dejará de tentarme el infierno, pero Tú eres más fuerte que el infierno. Sé que si siempre a Ti me encomiendo, jamás me separaré de Ti, y esta es la única gracia que te pido: haz que nunca cese de rogarte como ahora lo hago. Asísteme Señor, dame la luz, la fuerza, la perseverancia, dame el paraíso. Sobre todo dame tu amor, que es el paraíso de las almas. Te amo, bondad infinita, y quiero amarte siempre; escúchame, por amor a Jesucristo. " +
                "María, Tú que eres el refugio de los pecadores, socorre a uno que quiere amar sinceramente a nuestro Dios.<br>" +
                "Dulcísimo Señor del Milagro, perdona mis pecados, y libra, por tu misericordia, al pueblo de Salta y a tus devotos de todo castigo. Concédenos esta gracia, por intercesión  de  nuestra  Protectora,  tu  dulcísima  Madre,  la  Inmaculada  Virgen  del Milagro. <b>AMÉN</b>.<br></p>" +

                titulo1() +
                atributo("SOL", "Purísima Virgen del Milagro, María, Madre admirable, milagro de la gracia, el segundo atributo que simboliza tu original pureza es el  Sol. Alcánzame, Soberana Reina, de tu Santísimo Hijo, Sol de justicia, que con los rayos de su divina piedad ilumine las tinieblas en que camina perdida mi alma, para que, conociendo la ceguera en que he vivido, sepa llorar mis culpas, y al calor de tus cariños, se deshagan en lágrimas mis ojos; pues siendo Tú mi Reina y protectora, me atreví a ofenderte y a despreciar tu gloria. Que, purificada mi alma con la contrición de mis culpas, merezca ver en la gloria, el verdadero Sol de Justicia que nació de Ti, Jesucristo, Nuestro Señor. <b>AMÉN</b>.") +
               fin();
    }

    public String dia3() {
        return inicio() +
                tituloDia("TERCERO") +
                citas("Romanos 13, 8-14 / Evangelio según san Lucas 7, 36-50 / 1ª Carta del apóstol san Juan") +
                tituloOracion() +
                "<p>" +
                "¡Jesús Redentor mío! Te doy gracias de que no hayas permitido que muriese cuando estaba  en  desgracia. ¡Cuántos años seguidos merecía  estar  sepultado en  el abismo del infierno! Si hubieses muerto tal " +
                "día, aquella noche, ¿qué hubiera sido de mí por toda una eternidad? Señor, te doy gracias mil veces por este beneficio. Acepto la muerte en reparación de mis pecados; la acepto como Tú quieras mandármela; "+
                "ya que me has esperado hasta ahora, retárdala un poco más: Dios mío, déjame pues, que llore "+
                "mi dolor. Dame tiempo para llorar las ofensas de que me hice culpable a tus ojos, antes de que llegue el día en que has de juzgarme. No quiero resistirme ya por más tiempo a tu voz. ¡Quién sabe si las "+
                "palabras que acabo de oír son el último llamado que me haces escuchar! Confieso que soy indigno de misericordia. Tantas veces me has perdonado y yo, ingrato, te he ofendido de nuevo. Tú no desprecias el "+
                "corazón contrito y humillado (Sal 50, 19b). Señor, ya que no desechas un corazón que se arrepiente y se humilla, mira aquí a un pecador que vuelve a Ti, herido por el arrepentimiento. No me arrojes lejos de "+
                "tu presencia (Sal 50, 13). Por piedad, no me arrojes de tu presencia. Tú mismo dijiste: Al que venga a mí, yo no lo rechazaré (Jn 6, 37b). Verdad es que más que nadie te he ofendido, porque más que a nadie "+
                "me has favorecido con tus luces y tus gracias. La sangre que por mí has derramado, me da aliento, y me hace esperar el perdón, si verdaderamente me arrepiento. Sí, mi Soberano Bien, yo me arrepiento con "+
                "toda mi alma de haberte despreciado. Perdóname, y concédeme la gracia de amarte en adelante. Harto estoy ya de haberte ofendido. El tiempo que me queda por vivir, dulce Jesús mío, no quiero emplearlo " +
                "más en ofenderte; si no llorar amargamente por los disgustos que he podido darte. Amarte quiero con toda la fuerza de mi alma. ¡Dios, mereces un amor infinito! "+
                "¡María, mi esperanza, ruega a Jesús por mí!<br>"+
                "Dulcísimo Señor del Milagro, perdona mis pecados, y libra, por tu misericordia, a la ciudad de Salta y a tus devotos de todo castigo. Concédenos esta gracia, por intercesión  de  nuestra  Protectora,  tu "+
                "dulcísima  Madre,  la  Inmaculada  Virgen  del Milagro. "+
                "<b>AMÉN</b>."+
                "</p>" +
                titulo1() +
                atributo("ESTRELLA", "Purísima Virgen del Milagro, María, Madre admirable, milagro de la gracia, el tercer atributo que simboliza tu original pureza es la Estrella de Jacob. Eres Estrella resplandeciente que en la oscura noche de esta vida, alumbras con tus luces a los que perdidos caminan. Ves, piadosísima Reina y Estrella de pecadores, el camino que llevan mis pasos; actúa con tus benignas influencias, para que camine, seguro por el sendero verdadero que conduce a la gloria, que es el de la cruz y mortificación, y así, viviendo crucificado al mundo y a mis pasiones, merezca por tu intercesión ser estrella resplandeciente en la gloria. <b>AMÉN</b>") +
                fin();
    }

    public String dia4() {
        return inicio() +
                tituloDia("CUARTO") +
                citas("Carta a los Filipenses 2, 3-11 / Evangelio según san Juan 3, 16-21") +
                tituloOracion() +
                "<p>" +
               "Jesús mío, quiero siempre llamarte por tu nombre. Me consuela y me da valor, cuando me acuerdo que eres mi Salvador y que has muerto para salvarme. Mírame a tus pies, confieso que soy digno de tantos "+
                "infiernos, como veces te he ofendido por el pecado mortal. No merezco perdón; pero Tú has muerto para perdonarme. Piadoso Jesús, no olvides que por mí fue tu venida. Adelántate, Jesús mío, a perdonarme "+
                "antes que vengas a juzgarme. Entonces no podré pedirte piedad; pero ahora puedo, y espero que me la concedas. Entonces tus llagas me llenarán de espanto, ahora me inspiran "+
                "confianza. ¡Redentor de mi alma!, me arrepiento sobre todo de haber ofendido a tu infinita bondad, y prefiero sufrir todas las pérdidas posibles, antes que burlarme de tu gracia. Te amo con todo mi corazón, "+
                "ten piedad de mí. ¡Ten piedad de mí, Señor, por tu bondad, por tu gran compasión! (Sal 50, 3). "+
                "María, Madre de misericordia, abogada de los pecadores, alcánzame un intenso dolor de mis pecados, el perdón y la perseverancia en el divino amor. Te amo, Reina de mi corazón, y en Ti pongo toda mi confianza.<br>"+
                "Dulcísimo Señor del Milagro, perdona mis pecados, y libra, por tu misericordia, a la ciudad de Salta y a tus devotos de todo castigo. Concédenos esta gracia, por intercesión  de  nuestra  Protectora,  tu "+
                "dulcísima  Madre,  la  Inmaculada  Virgen  del Milagro.  "+
                "<b>AMÉN</b>."+
                "</p>" +
                titulo1() +
                atributo("ARCA DEL TESTAMENTO", "Purísima Virgen del Milagro, María, Madre admirable, milagro de la gracia, el cuarto atributo que simboliza tu original pureza es el Arca del Testamento. Eres Arca divina que, para que no pereciésemos en el diluvio de nuestras culpas, bajaste al pie del altar  para  asegurarnos en  Jesús Sacramentado,  concédeme,  Madre mía,  el  que,  no hallando descanso en este mundo sino en Jesús Sacramentado, se aquieten nuestras potencias y sentidos, para que gustando las dulzuras de este Pan Soberano, sienta aún en esta vida las delicias y gozos, que dan a los que te sirven en la bienaventuranza de la gloria. <b>AMÉN</b>") +
                fin();
    }

    public String dia5() {
        return inicio() +
                tituloDia("QUINTO") +
                citas("Romanos 8. 35-39 / Evangelio según san Mateo 22, 34-40 / 1ª Carta del apóstol san Juan 4, 7-21") +
                tituloOracion() +
                "<p>" +
                "¡Dios mío, Tú eres mi Bien infinito, y te he perdido tantas veces! ¡Sabía que por el pecado te causaría el mayor disgusto, pues perdería la gracia, y a pesar de esto, yo lo cometía! ¡Si yo no te viese clavado en "+
                "una cruz, Hijo de Dios, muriendo por mí, no me animaría a invocarte, ni a esperar jamás el perdón! ¡Padre Eterno, no fijes en mí tus ojos sino en tu Hijo querido, que te está clamando misericordia por mí: "+
                "escúchale y perdóname! Muchos años hace que debiera hallarme sepultado en el infierno, y sin  "+
                "esperanza de amarte y de recobrar la gracia que he perdido. ¡Dios mío! me arrepiento de la  ofensa  que  te  hice  renunciando  a  tu  amistad,  y  despreciando  tu  amor  por  los miserables placeres de este "+
                "mundo. ¡Hubiese deseado morir mil veces, antes que ofenderte! ¿Cómo pudo llegar a tal extremo mi ceguera y locura? Te agradezco, Dios mío, haberme dado tiempo para poder pensar en el mal que hice, y ya "+
                "que por tu misericordia no me hallo en el infierno y puedo amarte y amarte quiero, Dios mío, y no quiero demorar un solo instante de convertirme a Ti. Te amo, bondad infinita; te amo, vida mía, mi tesoro, mi "+
                "amor, mi todo. Recuérdame siempre el amor que me tuviste y el infierno donde debía encontrarme, a fin de que esto me obligue a hacer actos de amor y decirte siempre: Yo te amo. ¡María, Reina de mi "+
                "corazón, esperanza mía, Madre mía, si me hallara en el infierno, no podría amarte jamás! Yo te amo, Madre mía; en Ti pongo toda mi confianza, y espero no abandonarlos más, ni a Ti, ni a mi Dios. Socórreme "+
                "y ruega por mí a Jesús.<br>"+
                "Dulcísimo Señor del Milagro, perdona mis pecados, y libra, por tu misericordia, a la ciudad de Salta de todo castigo. Concédenos esta gracia, por intercesión de nuestra Protectora, tu dulcísima Madre, la "+
                "Inmaculada Virgen del Milagro."+
                "<b>AMÉN</b>."+
                "</p>" +
                titulo1() +
                atributo("PALOMA", "Purísima Virgen del Milagro, María, Madre admirable, milagro de la gracia, el quinto atributo que simboliza tu original pureza es la Paloma, que, volando a nuestra tierra, trajo el ramo de olivo para asegurar a los suyos que habían cesado ya, por tu intercesión, las aguas de las tribulaciones. Concédeme, Madre mía, que, como paloma que gime por la pérdida de su consorte, sepa llorar y sentir las muchas culpas con que he perdido a mi dulce Jesús, Esposo de mi alma, y que agradecido lave con mis lágrimas, las manchas con que he afeado mi alma, para que, vestido con la candidez de tu gracia, vuele en compañía de tu Esposo, el Espíritu Divino, a alabarte en la Gloria. <b>AMÉN</b>") +
                fin();
    }

    public String dia6() {
        return inicio() +
                tituloDia("SEXTO") +
                citas("Gálatas 6, 7-18 / Evangelio según san Lucas 12, 39-46 / Evangelio según san Lucas  23, 39-43") +
                tituloOracion() +
                "<p>" +
                "¡Jesús mío! ¿Cómo has podido sufrirme tanto tiempo? ¡Tantas veces he huido de Ti, y a pesar de esto, has venido siempre a mi encuentro! ¡Tantas veces te he ofendido y me has perdonado! ¡Te ofendí de nuevo "+
                "y de nuevo me has concedido el perdón! Hazme sentir un poco del dolor que padeciste en el Huerto de Getsemaní, cuando, por el peso de nuestros pecados, llegaste a sudar sangre. Me arrepiento, Redentor "+
                "mío, de haber correspondido tan mal a tu amor. ¡Placeres malditos, yo los detesto y abomino! Ustedes "+
                "me hicieron perder la gracia del Señor. ¡Amado Jesús!, te amo por sobre todas las cosas y renuncio a todas las satisfacciones ilícitas prefiriendo morir mil veces en lugar de ofenderte jamás. Por el afecto que "+
                "me mostraste en la Cruz, y que te obligó a ofrecer por mí esa vida divina, dame la luz y la fuerza para resistir las tentaciones, y recurrir en ellas a tu ayuda. "+
                "¡María, mi esperanza, ya que todo lo puedes ante Dios, alcánzame la santa perseverancia, y haz que no me separe jamás de tu amor!<br>"+
                "Dulcísimo Señor del Milagro, perdona mis pecados, y libra, por tu misericordia, a la ciudad de Salta y a tus devotos de todo castigo. Concédenos esta gracia, por intercesión  de  nuestra  Protectora,  tu "+
                "dulcísima  Madre,  la  Inmaculada  Virgen  del Milagro."+
                "<b>AMÉN</b>."+
                "</p>" +
                titulo1() +
                atributo("JARDÍN CERRADO", "Purísima Virgen del Milagro, María, Madre admirable, milagro de la gracia, el sexto atributo que simboliza tu original pureza es el Jardín Cerrado. Encierra, Madre mía, en tu corazón purísimo, como en jardín soberano, todos nuestros pensamientos y obras para que de hoy en adelante ya no piense en otra cosa, sino solo en servirte; ni siquiera más gloria que la de amarte. Haz que con la fragancia suavísima de todas tus virtudes se conviertan nuestros deseos en fruto de tu agrado, para que, adornándose mi alma con las flores de las virtudes, merezca ser suave olor de Cristo en la gloria. <b>AMÉN</b>") +
                fin();
    }

    public String dia7() {
        return inicio() +
                tituloDia("SEPTIMO") +
                citas("1 Jn 3, 14-18 / Evangelio según san Juan 15, 9-17") +
                tituloOracion() +
                "<p>" +
                "Mira, Dios mío; mira a tus pies un ingrato a quien creaste para el paraíso, pero que tantas veces, por miserables placeres, te ha negado y ha preferido ser condenado al infierno. Espero que Tú hayas perdonado "+
                "todas las ofensas que te hice, de las cuales me arrepiento de nuevo y quiero arrepentirme hasta la muerte. ¡Deseo que me las perdones todavía! Aunque Tú me hayas perdonado, no por esto será menos verdad "+
                "que tuve la audacia de llenarte de amargura, Redentor mío, que para conducirme a tu Reino me has dado la vida. ¡Bendita y glorificada sea para siempre, mi Jesús, tu misericordia!  "+
                "Tú con tanta paciencia me has sufrido, me has colmado de gracia y de luces y mil veces me has llamado a Ti. Veo, mi amado Jesús, que quieres que me salve; deseas que entre a tu Reino para amarte "+
                "eternamente; pero antes quieres que te ame en este mundo. Sí, quiero amarte; y aún cuando no hubiere paraíso, mientras viva, te amaré con todas mis fuerzas y con toda mi alma. Me basta saber, Dios mío, "+
                "que deseas que te ame. Jesús, asísteme con tu gracia, y no me abandones. Mi alma es inmortal: me hallo en la alternativa, o de amarte siempre o de detestarte por toda una eternidad. ¡Quiero amarte siempre!, "+
                "y amarte lo bastante en esta vida, para amarte en la eterna lo que debo. Dispón de mí como te plazca; corrígeme como Tú quieras,  pero no me prives de tu amor; haz después, de mí, lo que te parezca. ¡Jesús "+
                "mío! tus méritos son mi esperanza. "+
                "¡María, toda mi esperanza la pongo en tu intercesión! Tú me has librado del infierno cuando yo estaba en pecado. Ahora quiero ser de Dios; hazme santo y sálvame.<br>"+
                "Dulcísimo Señor del Milagro, perdona mis pecados, y libra, por tu misericordia, a la ciudad de Salta y a tus devotos de todo castigo. Concédenos esta gracia, por intercesión  de  nuestra  Protectora,  tu "+
                "dulcísima  Madre,  la  Inmaculada  Virgen  del Milagro. "+
                "<b>AMÉN</b>."+
                "</p>" +
                titulo1() +
                atributo("PUERTA DEL CIELO", "Purísima Virgen del Milagro, María, Madre admirable, milagro de la gracia, el séptimo atributo que simboliza tu original pureza, es la Puerta del Cielo. Eres Puerta Celestial, por cuya intercesión entran al paraíso de la gloria, los hijos de Adán que acaban esta vida en gracia. Concédeme, Madre mía, el que si este año fuese el último de mi vida, se aparte mi corazón de los cuidados de este mundo, y con la luz de tus auxilios busque solo el sosiego de mi alma en mi dulce Jesús, para que, cuando llegue la muerte temporal, merezca estar dispuesto para acabar mi vida en el beso de mi Señor, y entre por Ti, Puerta resplandeciente, a la Patria dichosa de la gloria. <b>AMÉN</b>.") +
                fin();
    }

    public String dia8() {
        return inicio() +
                tituloDia("OCTAVO") +
                citas("Hebreos 9, 11-15 / Hebreos 10, 19-31 / Evangelio según san Juan 6, 35-46") +
                tituloOracion() +
                "<p>" +
                "¡Bien supremo, soy el que he huido de Ti renunciando a tu amor! Por esto no soy digno de verte y de amarte. Pero Tú eres Aquel que por piedad de mí no la tuviste de Ti  mismo, y quisiste morir de dolor y "+
                "cubierto de infamia en una Cruz. Tu muerte me da  la esperanza  de  que  un  día  pueda  verte  y  gozar  de  tu  presencia,  amándote  con  todas  mis fuerzas. Ahora estoy  en  continuo peligro de perderte "+
                "para siempre,  y te he perdido por mis  pecados,  ¿qué  haré  durante  el  resto  de  mi  vida?  ¿Continuaré  ofendiéndote?  No, Jesús  mío,  yo  detesto  firmemente  los  ultrajes  que  te  he  hice;  estoy "+
                "arrepentido  de haberte ofendido y te amo de todo corazón. ¿Desecharías Tú un alma que se arrepiente y que  te  ama?  No.  Sé  que  has  dicho,  Redentor  mío,  que  no  sabes  rechazar  a  los  que  se "+
                "arrojan  a  tus  pies  arrepentidos: Al  que  venga  a  Mí,  Yo  no  lo  rechazaré  (Jn  6,  37b). ¡Jesús  mío,  todo  lo  abandono  y  me  convierto  a  Ti!  Te  abrazo  y  te  estrecho  contra  mi corazón;  dígnate "+
                "abrazarme  y  estrecharme  en  el  tuyo.  Si  me  atrevo  a  hablarte  así,  es porque  me  dirijo  a  la  bondad  infinita,  y  porque  hablo  a  un  Dios  que  ha  querido  morir por mi amor. ¡Salvador mío, dame la "+
                "esperanza en tu amor! ¡María,  querida  Madre  mía,  te  suplico  por  el  amor  que  tienes  a  Jesucristo, alcánzame la perseverancia! Así lo espero y así sea.<br>" +
                "Dulcísimo Señor del Milagro, perdona mis pecados y "+
                "libra, por tu misericordia, a la  ciudad  de  Salta  y  a  tus  devotos  de  todo  castigo.  Concédenos  esta  gracia,  por intercesión  de  nuestra  Protectora,  tu  dulcísima  Madre,  la  Inmaculada  Virgen  del "+
                "Milagro. "+
                "<b>AMÉN</b>."+
                "</p>" +
                titulo1() +
                atributo("FUENTE DE AGUAS VIVAS", "Purísima Virgen del  Milagro, María, Madre admirable, milagro de la gracia, el octavo atributo que simboliza tu original pureza, es el ser Fuente de Aguas. Concédeme, Madre  mía,  cual  sediento  ciervo  que  busca  las  aguas,  corra  a  beber  de  aquellas  cinco fuentes que por mí derramó mi dulce Jesús en el madero santo de la Cruz.  Que, atraído de  las  dulzuras  que  comunican  aquellas  santísimas  llagas,  lave  en  aquellas  purísimas aguas  las  muchas  manchas  con  que  he  afeado  mi  alma,  para  que,  cuando  venga  mi Señor  a  juzgarme  y  aparezca  en  el  cielo  aquel  madero  santo  de  la  Cruz,  llore  lágrimas de consuelo al ver que, aunque desprecié la Fuentes de Aguas vivas, la Cruz fue la llave que me abrió las puertas de la gloria. <b>AMÉN</b>") +
                fin();
    }

    public String dia9() {
        return inicio() +
                tituloDia("NOVENO") +
                citas("Efesios 1, 3-14 / Evangelio según san Juan 10, 10-18 / Evangelio según san Juan 17, 20-26") +
                tituloOracion() +
                "<p>" +
                "Te doy gracias, Redentor mío, por las luces que has dado,  y por haberme dado a conocer el camino de salvación. Te prometo ponerlos en práctica con la mayor firmeza. Concédeme la gracia que necesito para serte fiel; veo que tu voluntad es que me salve; y lo quiero, especialmente para dar gusto a tu Corazón divino que con tanto ardor desea mi salvación. No quiero resistir, Dios mío, por más tiempo al amor que me tienes. " +
                "Tu amor ha sido causa para que me hayas sufrido con tanta paciencia, cuando te ofendía. Tú me llamas a tu amor y no deseo sino amarte. Te amo, bondad infinita, y te suplico, por los méritos de Jesucristo, no permitas que sea jamás ingrato a tu bondad. Que cese de ser ingrato o déjame morir. Señor, ya que has empezado la obra, dígnate ahora completarla. ¡Se fuerte Dios, Tú que has actuado por nosotros! (Sal 67, 29b). Dame la luz, la fuerza, dame el amor. " +
                "¡María, Tú que eres la dispensadora de las gracias, socórreme! Acéptame como tu servidor, pues quiero serlo, y ruega a Jesús por mí. Los méritos de Jesucristo y tus ruegos, son los que han de salvarme.<br>" +
                "Dulcísimo Señor del Milagro, perdona mis pecados y libra, por tu misericordia, a la ciudad de Salta y a tus devotos de todo castigo. Concédenos esta gracia, por intercesión  de  nuestra  Protectora,  tu  dulcísima  Madre,  la  Inmaculada  Virgen  del Milagro."+
                "<b>AMÉN</b>."+
                "</p>" +
                titulo1() +
                atributo("TRONO", "Purísima Virgen del Milagro, María, Madre admirable, milagro de la gracia, el noveno atributo que simboliza tu original pureza, es ser Trono de Dios. Eres Trono donde descansó el Señor, como en trono de toda santidad y perfección. Concédeme, Madre mía, a mi corazón que camina perturbado con los engaños de este mundo, que descanse solo en Ti, y sienta aquel sosiego y alegría que experimentan tus siervos; nuestro amor te obligó a dejar tu trono y pedir que el Señor suspendiese el castigo contra el pueblo de Salta; por ello te suplico, Madre mía del Milagro, que continúe tu piedad y misericordia y suspenda los castigos que cada día merezco por mis culpas. Y si, frágil y miserable, me olvidase algún día de tu amor, Tú, que eres nuestro consuelo y amparo, llámame para que vuelva al rebaño de mi Señor; y por tu intercesión merezca verte en el trono de la gloria. <b>AMÉN</b>") +
                fin();
    }

    private String fin() {
        return titulo2() +
                oracion3() +
                oracion4() +
                doceEstrellasDelCieloDeMaria();
    }

    private String inicio(){
        return actoContriccion() +
                oracionTodosLosDias() ;
    }

    private String tituloDia(String dia) {
        return "<p style=\"text-align:center\"><b>DÍA " + dia + "</b><br></p>";
    }

    private String citas(String cita) {
        return "<p style=\"text-align:center\">" + cita + "</p>";
    }

    private String tituloOracion() {
        return "<p style=\"text-align:center\"><b>ORACIÓN</b><br><p>";
    }

    private String titulo1() {
        return "<p style=\"text-align:center\"><b>ATRIBUTOS DE MARÍA</b><p>";
    }

    private String atributo(String atributo, String oracion) {
        return "<p style=\"text-align:center\"><b>" + atributo + "</b><p>" +
                "<p>" + oracion + "</p>";
    }

    private String titulo2() {
        return "<p style=\"font-style:oblique\">Se pedirá lo que se desea conseguir.</p>";
    }

    private String actoContriccion() {
        return "<p style=\"text-align:center\"><b>ACTO DE CONTRICIÓN (1)</b></p>" +
                "<p>Dulce Jesús mío y mi crucificado Señor, indigno de ponerme delante de tus ojos, me postro avergonzado a tus pies, confesando la multitud de mis culpas, con íntimo dolor de mi alma por haberte ofendido. Herido vengo, médico divino, a buscar mi remedio en tu benigna misericordia, y te propongo con todo mi corazón la enmienda. Dulce amor mío eres sobre todas las cosas, ten piedad de mí; acuérdate, Señor,  que tu amor por mí, te puso en esa Cruz y no te acuerdes que yo, como ingrato y desconocido, me olvidé de tu paternal amor. Si a Ti, que eres mi Padre, no vuelvo los ojos, ¿quién otro se compadecerá de mí? ¡Señor Jesús cómo te ofendí! ¡Quién de dolor muriera a tus pies, pues amándome tanto me atreví a ofender a un Dios tan bueno, tan santo y tan amable! Pequé, Padre mío, contra el cielo y contra Ti, ten misericordia de mí. <b>AMÉN</b>.</p>";
    }

    private String oracionTodosLosDias() {
        return "<p style=\"text-align:center\"><b>ORACIÓN PREPARATORIA PARA TODOS LOS DÍAS (2)</b></p>" +
                "<p>María Purísima del Milagro, con tierno amor te inclinaste a pedir a tu Soberano Hijo, cuando enojado por nuestras culpas, quiso destruir la ciudad de Salta con aquellos espantosos terremotos. Tú, cual otra hermosa Ester, puesta delante del Supremo Rey de los Cielos, mudando de colores, pediste por la libertad de este pueblo. Concédeme, Madre mía del Milagro, que de tal suerte cambie mi vida, que si hasta aquí he caminado por los caminos de mi perdición olvidado de mi Dios y Señor, de hoy en adelante sólo reine en mi corazón tu maternal amor. Y que corresponda yo, amante y agradecido, a las obligaciones de hijo de tal Madre. No permitas, Madre mía, que se vea malograda en mí tu poderosa intercesión que todo lo puede conseguir, si no apartas tus purísimos ojos de este miserable pecador. Concédeme lo que te pido en esta novena, si es para mayor honra y gloria tuya, y bien de mi alma. <b>AMÉN</b>.</p>" +
                "<p style=\"font-style:oblique\">Se rezan tres Avemarías en honor a la Pura y Limpia Concepción del Milagro.</p>";
    }

    private String oracion3() {
        return "<p style=\"text-align:center\"><b>ORACIÓN (3)</b><p>" +
                "<p>Soberana Emperatriz de los cielos y la tierra, dulcísima Madre de pecadores, Madre del Milagro, en esta, tu elegida ciudad, en la cual muestras tu amor, mírame con semblante risueño. Aunque pecador y desagradecido, soy hijo tuyo, y te venero y amo como a Madre amorosa y admirable. Creo que si en mí empleas tus purísimos ojos, no me ha de desamparar mi Señor Jesucristo; porque a los que Tú tienes bajo tu patrocinio, Él les muestra especial amparo. Te imploro, Madre mía del Milagro, que no desprecies mis ruegos. Si cuando no te busqué como pecador, Tú solicitabas mi amistad porque deseabas mi salvación, ¿cómo ahora, que con tanta ansia te busco, me has de negar tu amparo, tu patrocinio y favor? Merezca yo tu poderoso brazo, ahora que arrodillado te pido me lleves de la mano a tu amado Hijo crucificado, para que, viendo mi dolor y arrepentimiento de mis culpas y pecados, que deseo sean mayores que los que han tenido los más penitentes Santos del mundo, me atraiga a Él y me dé a beber de aquella Sangre de su amoroso costado, que es todo el precio de nuestra redención, y viva sólo en Él, huyendo del mundo y de mi mismo. <b>AMÉN</b>.</p>" +
                "<p style=\"font-style:oblique\">Se reza un Credo a Cristo Crucificado.</p>";
    }

    private String oracion4() {
        return "<p style=\"text-align:center\"><b>ORACIÓN (4)</b></p><p>Amantísimo Jesús mío, hermosura eterna de la gloria, Tú eres mi Dios crucificado y todo mi bien. Justo Juez y piadoso Padre, no contento tu amor con haber bajado del cielo a la tierra a buscar al pecador; haber derramado tu sangre en el altar de la Cruz y haber instituido el Sacramento Eucarístico de tu Cuerpo y Sangre en la Santa Misa, quisiste venir en tu milagrosa imagen a esta ciudad de Salta, a buscar como Pastor Divino a la oveja perdida. Cuando más olvidada andaba de tu singular amor, hiciste estremecer la tierra con espantosos terremotos, y revelaste a tu siervo que no cesarían hasta que te sacasen por las calles. Te suplico, mi Dios crucificado, por tu mansedumbre sosiegues la inquietud de mi espíritu, para que pueda corresponder agradecido, buscándote sólo a Ti, descanso de mi alma y mi único bien. Si por haberte ofendido temblase mi alma de llegarse a Ti, dale voces desde esa Cruz, diciéndole: \"Mira, hijo mío, cuánto sufro por tu amor, y tú, ¿qué es lo que haces por Mí, sino solo ofenderme? Ven a mis brazos, que Yo clamaré a mi Eterno Padre diciendo: “Padre, perdona a este hijo ingrato, que no ha sabido lo que ha hecho al haber despreciado a su Dios y Redentor\" Si todavía tu amor retira de mí los ojos de su piedad por mi ignorancia e ingratitud, mira a tu Madre, María Santísima del Milagro, mi Protectora, por cuyos méritos y piadosa intercesión, espero se calmarán tus enojos, y me darás la gracia para que pueda servirte en esta vida y alabarte en la eterna. <b>AMÉN</b>.</p>";
    }

    private String doceEstrellasDelCieloDeMaria() {
        return "<p style=\"text-align:center\"><b>DOCE ESTRELLAS DEL CIELO DE MARÍA</b></p>" +
                "<p style=\"text-align:center\">I.<br>" +
                "Dios te salve, Madre<br>" +
                "Reina de los Cielos,<br>" +
                "esperanza nuestra<br>" +
                "refugio y consuelo.<br>" +
                "<br>" +
                "II.<br>" +
                "Virgen del Milagro,<br>" +
                "gloria de este pueblo,<br>" +
                "en quien siempre halla<br>" +
                "todo su remedio.<br>" +
                "<br>" +
                "III.<br>" +
                "Si son nuestras culpas<br>" +
                "muchas en extremo<br>" +
                "tus misericordias<br>" +
                "son más con exceso.<br>" +
                "<br>" +
                "IV.<br>" +
                "Ya el castigo estaba<br>" +
                "sobre nuestros yerros,<br>" +
                "más lo detuvieron<br>" +
                "tus piadosos ruegos.<br>" +
                "<br>" +
                "V.<br>" +
                "Al pie del sagrario<br>" +
                "allí intercediendo,<br>" +
                "el perdón pediste<br>" +
                "de nuestros excesos.<br>" +
                "<br>" +
                "VI.<br>" +
                "Mudando colores<br>" +
                "tu semblante bello<br>" +
                "a entender nos dio<br>" +
                "tu pena y consuelo.<br>" +
                "<br>" +
                "VII.<br>Empeñada estabas<br>" +
                "y echaste Tú el resto,<br>" +
                "para que el castigo<br>" +
                "no tuviese efecto.<br>" +
                "<br>" +
                "VIII.<br>" +
                "Perdona –decías–<br>" +
                "mi Dios a este pueblo,<br>" +
                "si no la corona<br>" +
                "de Reina aquí os dejo.<br>" +
                "<br>" +
                "IX.<br>" +
                "Yo por fiadora<br>" +
                "salgo es este empeño,<br>" +
                "y a mi cuenta corre<br>" +
                "no más ofenderos<br>" +
                "<br>" +
                "X.<br>" +
                "Confundirte quiso<br>" +
                "el dragón soberbio,<br>" +
                "pero con tu planta<br>" +
                "le quebraste el cuello.<br>" +
                "<br>" +
                "XI.<br>" +
                "Haz, Madre y Señora,<br>" +
                "que todos logremos<br>" +
                "el fruto, después<br>" +
                "de aqueste destierro.<br>" +
                "<br>" +
                "XII.<br>" +
                "En esta novena<br>" +
                "que humilde hacemos,<br>" +
                "nuestra petición<br>" +
                "por tu amor logremos.<br>";
    }
}
