package com.umayuxsolutions.novenamilagro.helpers;

import android.content.SharedPreferences;

import com.umayuxsolutions.novenamilagro.Novena;
import com.umayuxsolutions.novenamilagro.texts.Clasica;

public class PreferenceHelper {
    private static PreferenceHelper instance;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    private static final String PREF_NAME = "NOVENA";

    // Shared preferences file name
    private static final String BRIGHTNESS = "BRIGHTNESS";
    private static final String ZOOM_LEVEL = "ZOOM_LEVEL";
    private static final String VERSION = "VERSION";

    public PreferenceHelper() {
        pref = Novena.getInstance().getSharedPreferences(PREF_NAME, 0);
        editor = pref.edit();
    }

    public static PreferenceHelper getInstance() {
        if (instance == null) {
            instance = new PreferenceHelper();
        }
        return instance;
    }

    public boolean getBrightness() {
        return pref.getBoolean(BRIGHTNESS, true);
    }

    public void setBrightness(boolean brightness) {
        editor.putBoolean(BRIGHTNESS, brightness);
        editor.commit();
    }

    public int getZoomLevel() {
        return pref.getInt(ZOOM_LEVEL, 16);
    }

    public void setZoomLevel(int zoomLevel) {
        editor.putInt(ZOOM_LEVEL, zoomLevel);
        editor.commit();
    }

    public String getVersion() {
        return pref.getString(VERSION, Clasica.class.getName());
    }

    public void setVersion(String version) {
        editor.putString(VERSION, version);
        editor.commit();
    }

    public void reset() {
        setBrightness(true);
        setZoomLevel(16);
    }
}
